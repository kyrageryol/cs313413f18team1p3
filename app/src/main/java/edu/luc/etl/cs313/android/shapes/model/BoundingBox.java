package edu.luc.etl.cs313.android.shapes.model;

/**
 * A shape visitor for calculating the bounding box, that is, the smallest
 * rectangle containing the shape. The resulting bounding box is returned as a
 * rectangle at a specific location.
 */
public class BoundingBox implements Visitor<Location> {

	// TODO entirely your job (except onCircle)

	@Override
	public Location onCircle(final Circle c) {
		final int radius = c.getRadius();
		return new Location(-radius, -radius, new Rectangle(2 * radius, 2 * radius));
	}

	@Override
	public Location onFill(final Fill f) {

		return f.getShape().accept(this);
	}

	@Override
	public Location onGroup(final Group g) {
		Location l1 = g.getShapes().get(0).accept(this);
		for (int i = 1; i < g.getShapes().size(); i++)	{
			Location l2 = g.getShapes().get(i).accept(this);

			//x and y coordinates for location 1 (l1) and location 2 (l2)
			int x1 = l1.getX();
			int x2 = l2.getX();
			int y1 = l1.getY();
			int y2 = l2.getY();

			//upper left corner
			int upperLeftX = Math.min(x1,x2);
			int upperLeftY = Math.min(y1,y2);

			//cast location 1 and 2
			Rectangle rectL1 = (Rectangle) l1.getShape();
			Rectangle rectL2 = (Rectangle) l2.getShape();

			//get points for location 1 and 2
			int Lx1 = x1 + rectL1.getWidth();
			int Ly1 = y1 + rectL1.getHeight();
			int Lx2 = x2 + rectL2.getWidth();
			int Ly2 = y2 + rectL2.getHeight();

			// max value for Lower right point
			int lowerRightX = Math.max(Lx1,Lx2);
			int lowerRightY = Math.max(Ly1,Ly2);

			//Final width and height by subtracting Lower right from Upper left point
			int WIDTH = lowerRightX - upperLeftX;
			int HEIGHT = lowerRightY - upperLeftY;

			//set Bound for the groups
			l1 = new Location(upperLeftX,upperLeftY, new Rectangle(WIDTH,HEIGHT));

		}
		return l1;
	}

	@Override
	public Location onLocation(final Location l) {
		Location temp = l.getShape().accept(this);
		return new Location (l.getX()+temp.getX(),l.getY()+temp.getY(),temp.getShape());
	}

	@Override
	public Location onRectangle(final Rectangle r) {
		final int height = r.getHeight();
		final int width = r.getWidth();
	    return new Location (0,0, new Rectangle(width,height));
	}

	@Override
	public Location onStrokeColor(final StrokeColor c) {
		return c.getShape().accept(this);
	}

	@Override
	public Location onOutline(final Outline o) {
		return o.getShape().accept(this);
	}

	@Override
	public Location onPolygon(final Polygon s) {
		return onGroup(s);
	}
}
